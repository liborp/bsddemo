package cz.prenek.bsc.demo

import cz.prenek.bsc.demo.ExpenseItem
import cz.prenek.bsc.demo.ExpenseService
import cz.prenek.bsc.demo.ExpenseServiceImpl
import spock.lang.Specification

class ExpenseServiceImplTest extends Specification {

    ExpenseService expenseService

    def setup() {
        expenseService = new ExpenseServiceImpl(null, null)
    }

    def "compute expenses per currency"() {

        given:
        def expenses = [new ExpenseItem(100, "USD"),
                        new ExpenseItem(3, "JPN"),
                        new ExpenseItem(-10, "JPN"),
                        new ExpenseItem(0, "JPN"),
                        new ExpenseItem(50, "CZK"),
                        new ExpenseItem(-50, "CZK"),
                        new ExpenseItem(-90, "USD")]

        when:
        def output = expenseService.getCalculatedExpenses(expenses)

        then:
        output
        output.size() == 2 //no CZK as result is zero
        output.'USD' == 10
        output.'JPN' == -7

    }

}
