package cz.prenek.bsc.demo.tools

import cz.prenek.bsc.demo.ExpenseItem
import spock.lang.Specification

import java.text.ParseException

class ExpenseParserImplTest extends Specification {

    ExpenseParser parser

    def setup() {
        parser = new ExpenseParserImpl()
    }

    def "parser should return correct values"() {

        when:
        def output = parser.parse(input)

        then:
        output
        output == result

        where:
        input      || result
        "usd 100"  || new ExpenseItem(100, "USD")
        "JPN3 "    || new ExpenseItem(3, "JPN")
        " CZK -66" || new ExpenseItem(-66, "CZK")
        " AAA 0"   || new ExpenseItem(0, "AAA")
    }

    def "parser should provide parse exception"() {
        when:
        parser.parse("23 USD")

        then:
        ParseException ex = thrown()
        ex.message == "Cannot parse text '23 USD'"

    }
}
