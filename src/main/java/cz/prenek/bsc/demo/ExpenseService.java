package cz.prenek.bsc.demo;

import java.text.ParseException;
import java.util.List;

public interface ExpenseService {
    boolean addExpense(String value) throws ParseException;

    List<ExpenseItem> getAll();

    String getAllAsString();
}
