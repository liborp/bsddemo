package cz.prenek.bsc.demo.cmd;

import cz.prenek.bsc.demo.ExpenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.text.ParseException;

@ShellComponent
public class ExpenseCommand {

    private ExpenseService expenseService;

    @Autowired
    public ExpenseCommand(ExpenseService expenseService) {
        this.expenseService = expenseService;
    }

    @ShellMethod(value = "Adding new expense to system", group = "expense")
    public void add(String value1, String value2){
        String value = value1+value2;
        boolean resultOperation = true;
        try {
            resultOperation = expenseService.addExpense(value);
        } catch (ParseException e) {
            System.out.println(String.format("Cannot parse '%s' as expense, please use format like 'USD 100'",value));
            return;
        }

        if(resultOperation){
             System.out.println(" Added value " + value);
        }else{
            System.out.println(" Expense was not added");
        }
    }

    @ShellMethod(value = "Show all expenses", group = "expense")
    public void show(){
        System.out.println( expenseService.getAllAsString());
    }

}
