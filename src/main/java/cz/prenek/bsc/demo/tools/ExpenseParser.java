package cz.prenek.bsc.demo.tools;

import cz.prenek.bsc.demo.ExpenseItem;

import java.text.ParseException;

public interface ExpenseParser {

    ExpenseItem parse(String value) throws ParseException;
}
