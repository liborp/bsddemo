package cz.prenek.bsc.demo.tools;

import cz.prenek.bsc.demo.ExpenseItem;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class ExpenseParserImpl implements ExpenseParser {

    private static final Pattern REG_EXP = Pattern.compile("([A-Z]{3})[\\s]?([-0-9]*)");

    public ExpenseItem parse(String value) throws ParseException {

        Matcher matcher = REG_EXP.matcher(value.toUpperCase().trim());

        if (!matcher.matches()) {
            throw new ParseException(String.format("Cannot parse text '%s'", value), 0);
        }

        String currency = matcher.group(1);
        int amount = Integer.parseInt(matcher.group(2));

        return new ExpenseItem(amount,currency);
    }
}
