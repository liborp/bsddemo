package cz.prenek.bsc.demo.tools;

import cz.prenek.bsc.demo.ExpenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Scheduler {

    private ExpenseService expenseService;

    @Autowired
    public Scheduler(ExpenseService expenseService) {
        this.expenseService = expenseService;
    }


    @Scheduled(fixedRate = 60 * 1000)
    public void everyMinute() {
        System.out.println("Automated print of expenses:");
        System.out.println(expenseService.getAllAsString());
    }
}
