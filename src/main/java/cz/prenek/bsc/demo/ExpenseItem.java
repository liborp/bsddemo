package cz.prenek.bsc.demo;

import java.util.Objects;

public class ExpenseItem {

    private final int amount;

    private final String currency;

    public ExpenseItem(int amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public int getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getDisplayableValue() {
        return String.format("%s %d", currency, amount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExpenseItem that = (ExpenseItem) o;
        return amount == that.amount &&
                Objects.equals(currency, that.currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, currency);
    }

    @Override
    public String toString() {
        return "ExpenseItem{" +
                "amount=" + amount +
                ", currency='" + currency + '\'' +
                '}';
    }
}
