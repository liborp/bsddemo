package cz.prenek.bsc.demo;

import cz.prenek.bsc.demo.dao.ExpenseDAO;
import cz.prenek.bsc.demo.tools.ExpenseParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ExpenseServiceImpl implements ExpenseService {

    private ExpenseParser expenseParser;

    private ExpenseDAO expenseDAO;

    @Autowired
    public ExpenseServiceImpl(ExpenseParser expenseParser, ExpenseDAO expenseDAO) {
        this.expenseParser = expenseParser;
        this.expenseDAO = expenseDAO;
    }

    @Override
    public boolean addExpense(String value) throws ParseException {
        boolean result = false;
        ExpenseItem item = expenseParser.parse(value);
        if (validateItem(item)) {
            expenseDAO.add(item);
            result = true;
        }

        return result;
    }

    @Override
    public List<ExpenseItem> getAll() {
        return expenseDAO.getAll();
    }

    @Override
    public String getAllAsString() {
        StringBuilder result = new StringBuilder();
        Map<String, Integer> expensesByCurrency = getCalculatedExpenses(expenseDAO.getAll());

        expensesByCurrency.forEach((key,value) -> result.append(key).append(" ").append(value).append("\n"));

        if(result.length() == 0){
            result.append("No expenses available");
        }
        return result.toString();
    }

    private Map<String, Integer> getCalculatedExpenses(List<ExpenseItem> expenses) {
        Map<String, Integer> result = expenses.stream().collect(Collectors.toMap(ExpenseItem::getCurrency, ExpenseItem::getAmount, (oldValue, newValue) -> oldValue + newValue));
        result.values().removeIf(value -> value == 0);
        return result;
    }

    private boolean validateItem(ExpenseItem item) {
        boolean isValid = true;

        if(item.getAmount() == 0){
            isValid = false;
        }

        return  isValid;
    }


}
