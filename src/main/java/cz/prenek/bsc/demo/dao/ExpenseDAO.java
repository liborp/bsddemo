package cz.prenek.bsc.demo.dao;

import cz.prenek.bsc.demo.ExpenseItem;

import java.util.List;

public interface ExpenseDAO {

    void add(ExpenseItem item);

    List<ExpenseItem> getAll();
}
