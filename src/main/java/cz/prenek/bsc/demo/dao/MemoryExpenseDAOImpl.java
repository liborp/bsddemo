package cz.prenek.bsc.demo.dao;

import cz.prenek.bsc.demo.ExpenseItem;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class MemoryExpenseDAOImpl implements ExpenseDAO {

    private final List<ExpenseItem> items = Collections.synchronizedList(new ArrayList<>());

    @Override
    public void add(ExpenseItem item) {
        items.add(item);
    }

    @Override
    public List<ExpenseItem> getAll() {
        return Collections.unmodifiableList(items);
    }
}
