package cz.prenek.bsc.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.stream.Stream;

@Component
public class ApplicationInitializer implements InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationInitializer.class);

    @Value("${cz.prenek.bsc.demo.init.file.path}")
    private String expensesFilePath;

    private ExpenseService expenseService;

    @Autowired
    public ApplicationInitializer(ExpenseService expenseService) {
        this.expenseService = expenseService;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if(expensesFilePath == null || !new File(expensesFilePath).exists()){
            System.out.println("No file with expenses has been loaded.");
            return;
        }

        try (Stream<String> lines = Files.lines(Paths.get(expensesFilePath))) {
            lines.forEach(expense -> {
                try {
                    expenseService.addExpense(expense);
                } catch (ParseException e) {
                    logger.error(String.format("Cannot parse expense from file '%s'", expensesFilePath));
                }
            });
        }
    }
}
