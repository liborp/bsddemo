# HOMEWORK FOR BACKEND DEVELOPER POSITION IN BSC
by Libor Preněk

### Compile
The project uses Gradle for manage dependencies and build lifecycle. There should by wrapper of Gradle so no installation of Gradle is needed. 

Please look at [Official Gradle documentation](https://docs.gradle.org)

### Run
* after clone of project, please run: 'gradlew build jar'.
* go to <PROJECT DIR>/build/libs
* run java -jar <NAME OF JAR>

You can define you own application.properties, just create file with such name in directory with project JAR file.
Them you can define list of expenses for loading, please use key `cz.prenek.bsc.demo.init.file.path`

###Help
Once app has started, write 'help' for list of commands.
There are basically just two of them:

* add - adds new expense with formal 'add USD 100'
* show - list of all expenses, same format as automatic printing every minute.
* quit, exit - build in command for quit app


